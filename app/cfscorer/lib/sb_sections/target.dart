import 'dart:convert' show utf8;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../ble/bluetooth.dart';

class Target extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TargetState();
  }
}

class TargetState extends State<Target> {
  TextEditingController targetController = new TextEditingController();

  var i = 0;

  void add(a) {
    setState(() {
      i = a;
      bluetooth(i);
      targetController.clear();
    });
  }

  void bluetooth(int a) {
    var encoded = utf8.encode('TAR$a');
    score(encoded);
    //print(encoded);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxWidth: 400),
      padding: const EdgeInsets.all(3.0),
      //margin: const EdgeInsets.all(2.0),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
      ),
      child: Column(
        children: [
          Text(
            'Target',
            style: TextStyle(fontSize: 20),
          ),
          Text(
            i.toString(),
            style: TextStyle(fontSize: 28),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 100.0,
                child: TextField(
                    controller: targetController,
                    //maxLength: 3,
                    maxLines: 1,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Enter target value',
                      contentPadding: const EdgeInsets.symmetric(
                        vertical: 2,
                        horizontal: 10,
                      ),
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                    style: TextStyle(height: 1)),
              ),
              Flexible(
                child: Container(
                  margin: const EdgeInsets.only(left: 5.0, right: 5.0),
                  child: ElevatedButton(
                    onPressed: () => add(int.parse(targetController.text)),
                    child: Text('Set'),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
