import 'dart:convert' show utf8;

import 'package:flutter/material.dart';

import '../ble/bluetooth.dart';

class Runs extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RunsState();
  }
}

class RunsState extends State<Runs> {
  var i = 0;

  void add(int a) {
    setState(() {
      if (a == 0) {
        i = 0;
      } else {
        i = i + a;
      }
      if (i < 0) {
        i = 0;
      }
      bluetooth(i);
    });
  }

  void bluetooth(int a) {
    var encoded = utf8.encode('RUN$a');
    score(encoded);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(minWidth: 410, maxWidth: 530),
      padding: const EdgeInsets.all(3.0),
      //margin: const EdgeInsets.all(2.0),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
      ),
      child: Column(
        children: [
          Text(
            'Runs',
            style: TextStyle(fontSize: 20),
          ),
          Text(
            i.toString(),
            style: TextStyle(fontSize: 28),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: Container(
                  margin: const EdgeInsets.only(left: 2.5, right: 2.5),
                  child: ElevatedButton(
                    onPressed: () => add(6),
                    child: Text('+6'),
                  ),
                ),
              ),
              Flexible(
                child: Container(
                  margin: const EdgeInsets.only(left: 2.5, right: 2.5),
                  child: ElevatedButton(
                    onPressed: () => add(4),
                    child: Text('+4'),
                  ),
                ),
              ),
              Flexible(
                child: Container(
                  margin: const EdgeInsets.only(left: 2.5, right: 2.5),
                  child: ElevatedButton(
                    onPressed: () => add(1),
                    child: Text('+1'),
                  ),
                ),
              ),
              Flexible(
                child: Container(
                  margin: const EdgeInsets.only(left: 2.5, right: 2.5),
                  child: ElevatedButton(
                    onPressed: () => add(0),
                    child: Text('0'),
                  ),
                ),
              ),
              Flexible(
                child: Container(
                  margin: const EdgeInsets.only(left: 2.5, right: 2.5),
                  child: ElevatedButton(
                    onPressed: () => add(-1),
                    child: Text('-1'),
                  ),
                ),
              ),
              Flexible(
                child: Container(
                  margin: const EdgeInsets.only(left: 2.5, right: 2.5),
                  child: ElevatedButton(
                    onPressed: () => add(-4),
                    child: Text('-4'),
                  ),
                ),
              ),
              Flexible(
                child: Container(
                  margin: const EdgeInsets.only(left: 2.5, right: 2.5),
                  child: ElevatedButton(
                    onPressed: () => add(-6),
                    child: Text('-6'),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
