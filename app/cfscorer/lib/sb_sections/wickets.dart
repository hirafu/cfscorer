import 'dart:convert' show utf8;

import 'package:flutter/material.dart';

import '../ble/bluetooth.dart';

class Wickets extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return WicketsState();
  }
}

class WicketsState extends State<Wickets> {
  var i = 0;

  void add(int a) {
    setState(() {
      if (a == 0) {
        i = 0;
      } else {
        i = i + a;
      }
      if (i < 0) {
        i = 0;
      }
      bluetooth(i);
    });
  }

  void bluetooth(int a) {
    var encoded = utf8.encode('WKT$a');
    score(encoded);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(minWidth: 180, maxWidth: 240),
      padding: const EdgeInsets.all(3.0),
      //margin: const EdgeInsets.all(2.0),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
      ),
      child: Column(
        children: [
          Text(
            'Wickets',
            style: TextStyle(fontSize: 20),
          ),
          Text(
            i.toString(),
            style: TextStyle(fontSize: 28),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: Container(
                  margin: const EdgeInsets.only(left: 5.0, right: 5.0),
                  child: ElevatedButton(
                    onPressed: () => add(1),
                    child: Text('+1'),
                  ),
                ),
              ),
              Flexible(
                child: Container(
                  margin: const EdgeInsets.only(left: 5.0, right: 5.0),
                  child: ElevatedButton(
                    onPressed: () => add(0),
                    child: Text('zero'),
                  ),
                ),
              ),
              Flexible(
                child: Container(
                  margin: const EdgeInsets.only(left: 5.0, right: 5.0),
                  child: ElevatedButton(
                    onPressed: () => add(-1),
                    child: Text('-1'),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
