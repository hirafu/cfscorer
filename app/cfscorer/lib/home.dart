import 'package:flutter/material.dart';

import './sb_sections/overs.dart';
import './sb_sections/runs.dart';
import './sb_sections/target.dart';
import './sb_sections/wickets.dart';
import './ble/bluetooth.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CFScorer'),
        actions: [
          IconButton(
            icon: Icon(Icons.bluetooth),
            onPressed: () {
              Navigator.pushNamed(context, '/scan');
            },
          )
        ],
      ),
      body: Container(
        margin: const EdgeInsets.all(10.0),
        width: double.infinity,
        child: SingleChildScrollView(
          child: Wrap(
            runSpacing: 15,
            spacing: 10,
            alignment: WrapAlignment.spaceEvenly,
            children: [
              Runs(),
              Wickets(),
              Overs(),
              Target(),
            ],
          ),
        ),
      ),
    );
  }
}
