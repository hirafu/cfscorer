import 'package:flutter_blue/flutter_blue.dart';

FlutterBlue ble = FlutterBlue.instance;

var globalChar;
var globalDevice;
bool bluetoothConnected = false;
bool globalConnected = false;
bool deviceConnected = false;

String buttonClick() {
  currentState();
  if (bluetoothConnected) {
    print(deviceConnected);
    if (deviceConnected) {
      globalDevice.disconnect();
      return ('Disconnected from device!');
    } else {
      connect();
      return ('Connected to deivce!');
    }
  } else {
    return ('Need to turn bluetooth on!');
  }
}

void currentState() {
  bluetoothState();
  deviceState();
}

void bluetoothState() {
  ble.state.listen((result) {
    if (result == BluetoothState.on) {
      bluetoothConnected = true;
    } else {
      bluetoothConnected = false;
    }
  });
}

void deviceState() {
  if (globalDevice == null) {
    deviceConnected = false;
  } else {
    globalDevice.state.listen((result) {
      if (result == BluetoothDeviceState.connected) {
        deviceConnected = true;
      } else {
        deviceConnected = false;
      }
    });
  }
}

void connect() {
  var i = 0;
  ble.startScan(
    timeout: Duration(seconds: 4),
    withServices: [Guid('5a0d6a15-b664-4304-8530-3a0ec53e5bc1')],
  );

  ble.scanResults.listen((results) async {
    for (ScanResult r in results) {
      if (r.device.name == 'CFScorer') {
        if (i == 0) {
          i = i + 1;
          await r.device.connect();
          globalDevice = r.device;
          deviceConnected = true;
          await servicescheck(r.device);
        }
      }
    }
  });
}

Future servicescheck(device) async {
  List<BluetoothService> services = await device.discoverServices();
  services.forEach((service) {
    service.characteristics.forEach((char) {
      if (char.uuid == Guid('73b0cf1d-eae2-470c-a24e-d48910d1c345')) {
        print(char);
        globalChar = char;
      }
    });
  });
}

void score(List<int> value) async {
  if (globalChar == null) {
    print('Global char is blank, fetching!');
    var list = await FlutterBlue.instance.connectedDevices;
    var device = list[0];
    servicescheck(device);
  }

  try {
    await globalChar.write(value, withoutResponse: true);
  } catch (err) {
    print(err);
  }
}
