import 'package:cfscorer/home.dart';
import 'package:cfscorer/scan.dart';
import 'package:flutter/material.dart';

import './home.dart';

void main() => runApp(Scorer());

class Scorer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(routes: {
      '/': (context) => HomeScreen(),
      '/scan': (context) => ScanScreen(),
    });
  }
}
