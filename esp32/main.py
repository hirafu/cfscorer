from micropython import const
from machine import Pin

import bluetooth
import struct
import time
#import aioble

#const flags

_ADV_TYPE_FLAGS = const(0x01)
_ADV_TYPE_NAME = const(0x09)
_ADV_TYPE_UUID16_COMPLETE = const(0x3)
_ADV_TYPE_UUID32_COMPLETE = const(0x5)
_ADV_TYPE_UUID128_COMPLETE = const(0x7)
_ADV_TYPE_UUID16_MORE = const(0x2)
_ADV_TYPE_UUID32_MORE = const(0x4)
_ADV_TYPE_UUID128_MORE = const(0x6)
_ADV_TYPE_APPEARANCE = const(0x19)

class Scoreboard(object):
    def receive(self, score, obj):
        
        global scoreboard
        
        if obj == 'runs':
            runsp = self.pad(score,3)
            scoreboard = runsp + scoreboard[3:]
        elif obj == 'wickets':
            scoreboard = scoreboard[:3] + score + scoreboard[4:]
        elif obj == 'overs':
            if score == '':
                s = int(scoreboard[4:6].strip())
                s = s + 1
                score = str(s)
            oversp = self.pad(score,2)
            scoreboard = scoreboard[:4] + oversp + scoreboard[6:]
        elif obj == 'target':
            #s = int(score) + 1
            #score = str(s)
            targetp = self.pad(score,3)
            scoreboard = scoreboard[:6] + targetp
        print(scoreboard)
        self.numberBinary(scoreboard)
    
    #makes the numbers the correct lenght
    def pad(self, number, m):
        l = len(number)
        while l < m:
            number = ' ' + number
            l = len(number)
        return number


    #Turn numbers into binary for shift register
    def numberBinary(self, result):
        num = {' ':(0b0000000),
        '0':(0b1111110),
        '1':(0b0110000),
        '2':(0b1101101),
        '3':(0b1111001),
        '4':(0b0110011),
        '5':(0b1011011),
        '6':(0b1011111),
        '7':(0b1110000),
        '8':(0b1111111),
        '9':(0b1111011)}

        #print it to text too so can see print results        
        nums = {' ':('0b0000000'),
        '0':('0b1111110'),
        '1':('0b0110000'),
        '2':('0b1101101'),
        '3':('0b1111001'),
        '4':('0b0110011'),
        '5':('0b1011011'),
        '6':('0b1011111'),
        '7':('0b1110000'),
        '8':('0b1111111'),
        '9':('0b1111011')}
        
        s = Shifter()
        """
        #single number
        rs = nums[result[2]]
        s.shift_out(num[result[2]])
        """
        #multi number
        rs = nums[result[0]]+nums[result[1]]+nums[result[2]]+nums[result[3]]+nums[result[4]]+nums[result[5]]+nums[result[6]]+nums[result[7]]+nums[result[8]]
        s.shift_out(num[result[0]], num[result[1]], num[result[2]], num[result[3]], num[result[4]], num[result[5]], num[result[6]], num[result[7]], num[result[8]])
        
        print(rs)

class BLEDevice(object):
    def __init__(self, ble):
        self.__ble = ble

        self.__ble.active(True)

            #Advertiser encoder
        def advertising_payload(limited_disc=False, br_edr=False, name=None, services=None, appearance=0):
            payload = bytearray()

            def _append(adv_type, value):
                nonlocal payload
                payload += struct.pack("BB", len(value) + 1, adv_type) + value

            _append(
                _ADV_TYPE_FLAGS,
                struct.pack("B", (0x01 if limited_disc else 0x02) + (0x18 if br_edr else 0x04)),
            )

            if name:
                _append(_ADV_TYPE_NAME, name)

            if services:
                for uuid in services:
                    b = bytes(uuid)
                    if len(b) == 2:
                        _append(_ADV_TYPE_UUID16_COMPLETE, b)
                    elif len(b) == 4:
                        _append(_ADV_TYPE_UUID32_COMPLETE, b)
                    elif len(b) == 16:
                        _append(_ADV_TYPE_UUID128_COMPLETE, b)

            # See org.bluetooth.characteristic.gap.appearance.xml
            if appearance:
                _append(_ADV_TYPE_APPEARANCE, struct.pack("<h", appearance))

            return payload

        # Advertiser

        self.__payload = advertising_payload(
            name='CFScorer', 
            services=[bluetooth.UUID('5a0d6a15-b664-4304-8530-3a0ec53e5bc1')],
        )

    def GATTServer(self):
        #GATT Server
        S_UUID = bluetooth.UUID('5a0d6a15-b664-4304-8530-3a0ec53e5bc1') #serviceid
        S_PC = (bluetooth.UUID('df531f62-fc0b-40ce-81b2-32a6262ea440'), bluetooth.FLAG_WRITE) #characteristic + flags for Play-cricket
        S_CF = (bluetooth.UUID('73b0cf1d-eae2-470c-a24e-d48910d1c345'), bluetooth.FLAG_WRITE) #characteristic + flags for companion
        S_SERVICE = (S_UUID, (S_PC,S_CF,),)
        SERVICES = (S_SERVICE,)
        ( (self.pc,self.cf,), ) = ble.gatts_register_services(SERVICES) 

    def advertise(self):
        self.__ble.gap_advertise(50000, adv_data=self.__payload, connectable=True)

    def ble_irq(self, event, data):
        #print received data
        #print(ble.gatts_read(cf))
        print(ble.gatts_read(self.pc).decode("utf-8"))
        val = ble.gatts_read(self.pc).decode("utf-8")
        cfval = ble.gatts_read(self.cf).decode("utf-8")
        sb = Scoreboard()
        #Play-cricket decode
        if val[:3] == "BTS":
                i = val.find("/")
                runs = val[3:i]
                wickets = val[i+1:len(val)]
                sb.receive(runs,"runs")
                sb.receive(wickets,"wickets")
        elif val[:3] == "OVB":
            i = val.find(".")
            if i == '':
                overs = val[3:]
            else:
                overs = val[3:i]
            sb.receive(overs,"overs")
        elif val[:3] == "FTS":
            target = val[3:]
            s = int(target) + 1
            score = str(s)
            sb.receive(score,"target")
        elif val[:3] == "DLT":
            target = val[3:]
            sb.receive(target,"target")

        #companion decode
        if cfval[:3] == "RUN":
            runs = cfval[3:]
            sb.receive(runs,"runs")
        elif cfval[:3] == "WKT":
            wickets = cfval[3:]
            sb.receive(wickets,"wickets")
        elif cfval[:3] == "OVR":
            overs = cfval[3:]
            sb.receive(overs,"overs")
        elif cfval[:3] == "TAR":
            target = cfval[3:]
            sb.receive(target,"target")
        #print(val)

        self.advertise()

class Shifter(object):

    """
    A class for controlling one or more shift registers.  When instantiated you
    can choose which pins to use for the *data_pin*, *latch_pin*. and,
    *clock_pin*.

    Optionally, if the *invert* keyword argument is ``True`` then all bitwise
    operations will be inverted (bits flipped) before being sent to the shift
    register.  This is a convenient way to invert things like LED matrices and
    deal with relays that are "active low" (i.e. all relays are on when the
    pins are low instead of high).

    .. note::

        You must ensure that the numbers you pass for the pins match the mode
        you're using with `RPi.GPIO`.
    """
    def __init__(self, data_pin=12, latch_pin=32, clock_pin=25, invert=False):
        self.data_pin = Pin(data_pin, Pin.OUT) #seriel in
        self.latch_pin = Pin(latch_pin, Pin.OUT) #Register Clock
        self.clock_pin = Pin(clock_pin, Pin.OUT) #Shift Register Clock
        self.invert = invert

    def shift_out(self, *values):

        bits = {"0": 0, "1": 1}
        if self.invert:
            bits = {"1": 0, "0": 1}
        self.latch_pin.value(0)
        for val in reversed(values):
            for bit in '{0:08b}'.format(val):
                self.clock_pin.value(0)
                self.data_pin.value(bits[bit])
                self.clock_pin.value(1)
        self.latch_pin.value(1)

    def test(self):
        import time
        i = 0
        """
        Performs a test of the shift register by setting each pin HIGH for .25
        seconds then LOW for .25 seconds in sequence.
        
        
        for i in range(7):
            self.shift_out(1 << i,1 << i,1 << i,1 << i,1 << i,1 << i,1 << i,1 << i,1 << i)
            time.sleep(0.5)
            self.shift_out(0,0,0,0,0,0,0,0,0)
            time.sleep(0.5
            )
        """
        sb = Scoreboard()
        
        
        while i < 10:
            s = str(i)
            n = s + s + s + s + s + s + s + s + s
            print(n)
            sb.numberBinary(n)
            i += 1
            time.sleep(0.4)
        self.all(0)
        
        
        
    
    def all(self, state=0):
        """
        Sets all pins on the shift register to the given *state*.  Can be
        either HIGH or LOW (1 or 0, True or False).
        """
        if state:
            self.shift_out(0b11111111)
        else:
            self.shift_out(0b00000000,0b00000000,0b00000000,0b00000000,0b00000000,0b00000000,0b00000000,0b00000000,0b00000000)

#Configuration of Bluetooth
ble = bluetooth.BLE()
scoreboardBluetooth = BLEDevice(ble)
scoreboardBluetooth.GATTServer()
scoreboardBluetooth.advertise()
#ble.active(True)

ble.irq(scoreboardBluetooth.ble_irq)

scoreboard = '         '
s = Shifter()
s.test()

##ble.gap_advertise(50000, adv_data=payload, connectable=True)


